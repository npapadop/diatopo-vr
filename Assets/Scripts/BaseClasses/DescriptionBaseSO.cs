using UnityEngine;

/// <summary>
/// Base class for scriptable objects that need a description field
/// </summary>
public class DescriptionBaseSO : ScriptableObject
{
    [TextArea] public string description;
}
