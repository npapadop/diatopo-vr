using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// Class responsible to raise event on scene loading
/// </summary>
public class OnSceneLoad : MonoBehaviour
{
    public UnityEvent OnLoad = new UnityEvent();

    public void OnEnable()
    {
        SceneManager.sceneLoaded += RaiseEvent;
    }

    public void OnDisable()
    {
        SceneManager.sceneLoaded -= RaiseEvent;
    }

    private void RaiseEvent(Scene scene, LoadSceneMode mode)
    {
        OnLoad.Invoke();
    }
}
