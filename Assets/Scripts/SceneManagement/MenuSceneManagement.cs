using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSceneManagement : MonoBehaviour
{
    [Header("Listening On")]
    [SerializeField] private VoidEventChannelSO _exitSceneChannel = default;

    [Header("Broadcasting On")]
    [SerializeField] private LoadLocationEventChannelSO _locationSelectedLoadChannel = default;
    [SerializeField] private int mediaPanelIndex = 0;

    [SerializeField] private float locationLoadDelay = 0.5f;

    private void OnEnable()
    {
        _exitSceneChannel.OnEventRaised += QuitApplication;
    }

    private void OnDisable()
    {
        _exitSceneChannel.OnEventRaised -= QuitApplication;
    }

    public void LocationToLoad(GameSceneSO gameSceneSO)
    {
        if (gameSceneSO == null)
        {
            Debug.Log("Select location button trigger not set to a location scene");
            return;
        }

        StartCoroutine(LoadLocationOnDelay(gameSceneSO));
        
    }

    private IEnumerator LoadLocationOnDelay(GameSceneSO gameSceneSO)
    {
        yield return new WaitForSeconds(locationLoadDelay);
        _locationSelectedLoadChannel.RaiseEvent(gameSceneSO, true, true, mediaPanelIndex);
    }

    public void QuitApplication()
    {
        // save any game data here
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif

        Application.Quit();


    }

}