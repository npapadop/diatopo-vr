using UnityEngine.AddressableAssets;

public class GameSceneSO : DescriptionBaseSO
{
    public GameSceneType sceneType;
    public AssetReference sceneReference;

    public enum GameSceneType
    {
                //Special Scenes
        Initialization,
        ApplicationManagers,
        //Playable Scenes
        Menu,
        Location,
        VideoContent,
        Video360Content,
        Model3DContent,
    }
}
