using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object containing information specific to Application Manager scene data
/// </summary>

//Menu creation is disable because we want to create only one application manager initialization
//[CreateAssetMenu(fileName = "ApplicationManagers", menuName = "Scene Data/ApplicationManagers")]
public class ApplicationManagersSO : GameSceneSO{}
