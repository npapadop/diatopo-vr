using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object containing information specific to Location scene data
/// </summary>

[CreateAssetMenu(fileName = "NewContent", menuName = "Scene Data/Content")]
public class ContentSO : GameSceneSO {}
