using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentSceneManagement : MonoBehaviour
{
    [Header("Listening On")]
    [SerializeField] private VoidEventChannelSO _exitSceneChannel = default;

    [Header("Broadcasting On")]
    [SerializeField] private LoadLocationEventChannelSO _locationLoadChannel = default;
    [SerializeField] private GameSceneSO _locationScene;
    [SerializeField] private int mediaPanelIndex = 0;

    private void OnEnable()
    {
        _exitSceneChannel.OnEventRaised += LocationToLoad;
    }

    private void OnDisable()
    {
        _exitSceneChannel.OnEventRaised -= LocationToLoad;
    }

    public void LocationToLoad()
    {
        if (_locationScene == null)
        {
            Debug.Log("Select location button trigger not set to a location scene");
            return;
        }

        _locationLoadChannel.RaiseEvent(_locationScene, true, true, mediaPanelIndex);
    }
}
