using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationSceneManagement : MonoBehaviour
{
    [Header("Listening On")]
    [SerializeField] private VoidEventChannelSO _exitSceneChannel = default;
    [SerializeField] private GameSceneSO menuSceneSO;

    [Header("Broadcasting On")]
    [SerializeField] private LoadLocationEventChannelSO _loadSceneChannel = default;
    [SerializeField] private LoadEventChannelSO _loadMenuChannel = default;

    void OnEnable()
    {
        _exitSceneChannel.OnEventRaised += LoadMenuScene;
    }

    void OnDisable()
    {
        _exitSceneChannel.OnEventRaised -= LoadMenuScene;
    }

    public void LoadContentScene(GameSceneSO gameSceneSO)
    {


        if (gameSceneSO == null)
        {
            Debug.Log("No scene selected");
            return;
        }

        _loadSceneChannel.RaiseEvent(gameSceneSO, true, true, 0);
    }

    private void LoadMenuScene()
    {
        if (menuSceneSO == null)
        {
            Debug.Log("No main menu scene selected");
            return;
        }

        _loadMenuChannel.RaiseEvent(menuSceneSO, false, true, false);
    }
}
