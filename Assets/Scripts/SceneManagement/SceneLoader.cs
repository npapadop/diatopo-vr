using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;

public class SceneLoader : MonoBehaviour
{
    [Header("Listening to")]
    [SerializeField] private LoadLocationEventChannelSO _loadLocation = default;
    [SerializeField] private LoadEventChannelSO _loadMenu = default;

    [Header("Broadcasting on")]
    [SerializeField] private FadeChannelSO _fadeRequestChannel = default;
    [SerializeField] private SceneEventChannelSO _onSceneReady = default;
    //[SerializeField] private BoolEventChannelSO _toggleLoadingScreen = default;
    
    //[SerializeField] private ControllersEventChannelSO _toggleControllersChannel = default;
    [SerializeField] private ControllersEventChannelSO _toggleBaseControllersChannel = default;
    [SerializeField] private ControllersEventChannelSO _toggleDirectControllersChannel = default;
    [SerializeField] private ControllersEventChannelSO _toggleTeleportControllersChannel = default;
    [SerializeField] private BoolEventChannelSO _toggleSnapTurnControllersChannel = default;
    [SerializeField] private BoolEventChannelSO _toggleTurnAroundControllerChannel = default;
    [SerializeField] private FloatEventChannelSO _rayLengthRequestChannel = default;
    [SerializeField] private VoidEventChannelSO _resetOriginRequestChannel = default;

    [SerializeField] private float _mainMenuRayLength = 1000;
    //[SerializeField] private float _locationRayLength = 30;
    [SerializeField] private float _contentRayLength = 30;

    [SerializeField] private GameObject quitPanel;

    [SerializeField] private ProgressLoadingController _progressLoadingController;
    [SerializeField] private DownloadSceneProgress _downloadSceneProgress;
    private AsyncOperationHandle<SceneInstance> _loadingOperationHandle;

    //Parameters coming from scene loading requests
    private GameSceneSO _sceneToLoad;
    private GameSceneSO _currentlyLoadedScene;
    private bool _showLoadingScreen;

    [SerializeField] private float _fadeDuration = 1.5f;
    [SerializeField] private float _fastFadeDuration = 0.5f;
    private bool _isLoading = false; //To prevent a new loading request while already loading a new scene

    private int _extraParameter = 0;

    private void OnEnable()
    {
        _loadMenu.OnLoadingRequested += LoadMenu;
        _loadLocation.OnLoadingRequested += LoadLocation;
        
    }

    private void OnDisable()
    {
        _loadMenu.OnLoadingRequested -= LoadMenu;
        _loadLocation.OnLoadingRequested -= LoadLocation;
    }

    private void Start()
    {
        _fadeRequestChannel.FadeOut(_fadeDuration);
    }

    private void LoadLocation(GameSceneSO locationToLoad, bool showLoadingScreen, bool fadeScreen, int extraParam)
    {
        if (_isLoading)
            return;

        _sceneToLoad = locationToLoad;
        _showLoadingScreen = showLoadingScreen;
        _isLoading = true;

        _extraParameter = extraParam;

        quitPanel.SetActive(false);
        DisableControllersInteraction();

        StartCoroutine(UnloadPreviousScene());

    }

    private void LoadMenu(GameSceneSO menuToLoad, bool showLoadingScreen, bool fadeScreen, bool firstLoad)
    {
        if (_isLoading)
            return;

        _sceneToLoad = menuToLoad;
        _showLoadingScreen = showLoadingScreen;
        _isLoading = true;

        _extraParameter = firstLoad?0:1;

        quitPanel.SetActive(false);
        DisableControllersInteraction();

        StartCoroutine(UnloadPreviousScene());
    }

    private IEnumerator UnloadPreviousScene()
    {
        _fadeRequestChannel.FadeOut(_fadeDuration);

        yield return new WaitForSeconds(_fadeDuration);

        if(_currentlyLoadedScene != null) //would be null if the game was started in Initialisation
        {
            if (_loadingOperationHandle.IsValid())
            { 
                
                Addressables.UnloadSceneAsync(_loadingOperationHandle, true).Completed += (op) =>
                {
                    if (op.Status == AsyncOperationStatus.Succeeded)
                    {
                        Debug.Log("Successfully unloaded scene");
                    }
                };
            }
        }

        LoadNewScene();
    }

    private void LoadNewScene()
    {
        if (_showLoadingScreen)
        {
            //_toggleLoadingScreen.RaiseEvent(true);
            _progressLoadingController.ToggleLoadingScreen(true);
            _fadeRequestChannel.FadeIn(_fastFadeDuration);
            _resetOriginRequestChannel.OnEventRaised();
        }

        StartCoroutine(DownloadScene());
    }

    private IEnumerator DownloadScene()
    {
        var downloadScene = Addressables.LoadSceneAsync(_sceneToLoad.sceneReference, LoadSceneMode.Additive, priority: 0);
        downloadScene.Completed += OnNewSceneLoaded;
        int progress = 0;

        if(_showLoadingScreen)
            _downloadSceneProgress.downloadProgressInput = progress;

        while (!downloadScene.IsDone)
        {
            var status = downloadScene.GetDownloadStatus();
            progress = (int)(status.Percent * 100);

            if (_showLoadingScreen)
                _downloadSceneProgress.downloadProgressInput = progress;

            yield return null;
        }     
    }

    private void OnNewSceneLoaded(AsyncOperationHandle<SceneInstance> _handle)
    {

        _downloadSceneProgress.downloadProgressInput = 100;
        _currentlyLoadedScene = _sceneToLoad;
        SceneManager.SetActiveScene(_handle.Result.Scene);
        _loadingOperationHandle = _handle;

        _isLoading = false;

        if (_showLoadingScreen)
        {
            //_toggleLoadingScreen.RaiseEvent(false);
            _progressLoadingController.ToggleLoadingScreen(false);
            _fadeRequestChannel.FadeOut(_fastFadeDuration);
        }

        quitPanel.SetActive(true);
        ToggleControllersFeatures();

        _fadeRequestChannel.FadeIn(_fadeDuration);
        _resetOriginRequestChannel.OnEventRaised();

        StartSceneGameplay();
    }

    private void StartSceneGameplay()
    {
        _onSceneReady.EventRaise(_currentlyLoadedScene, _extraParameter);
    }

    private void ToggleControllersFeatures()
    {
        //_toggleControllersChannel.RaiseEvent(true, true);

        //Debug.Log("Scene type to load: " + _sceneToLoad.sceneType);
        //Debug.Log("Scene name to load: " + _sceneToLoad.name);


        switch (_sceneToLoad.sceneType)
        {
            case GameSceneSO.GameSceneType.Menu:
                _toggleBaseControllersChannel.OnEventRaised(true, true);
                _rayLengthRequestChannel.OnEventRaised(_mainMenuRayLength);
                _toggleSnapTurnControllersChannel.RaiseEvent(true);
                break;
            case GameSceneSO.GameSceneType.Location:
                _toggleDirectControllersChannel.OnEventRaised(true, true);
                _toggleSnapTurnControllersChannel.RaiseEvent(true);
                break;
            case GameSceneSO.GameSceneType.VideoContent:
                _toggleBaseControllersChannel.OnEventRaised(true, true);
                _rayLengthRequestChannel.OnEventRaised(_contentRayLength);  
                _toggleSnapTurnControllersChannel.RaiseEvent(true);        
                break;
            case GameSceneSO.GameSceneType.Video360Content:
                _toggleDirectControllersChannel.OnEventRaised(true, true);
                _toggleSnapTurnControllersChannel.RaiseEvent(true);
                _toggleTurnAroundControllerChannel.RaiseEvent(true);
                break;
            case GameSceneSO.GameSceneType.Model3DContent:
                _toggleBaseControllersChannel.OnEventRaised(true, true);
                _toggleTeleportControllersChannel.OnEventRaised(true, true);
                _toggleSnapTurnControllersChannel.RaiseEvent(true); 
                _rayLengthRequestChannel.OnEventRaised(10f);       
                break;         
            default:
                Debug.Log("No valid game scene selected to activate control features");
                break;    
        }
    }

    private void DisableControllersInteraction()
    {
        //_toggleControllersChannel.OnEventRaised(false, false);
        _toggleBaseControllersChannel.OnEventRaised(false, false);
        _toggleDirectControllersChannel.OnEventRaised(false, false);
        _toggleTeleportControllersChannel.OnEventRaised(false, false);
        _toggleSnapTurnControllersChannel.RaiseEvent(false);
        _toggleTurnAroundControllerChannel.RaiseEvent(false);
    }
}
