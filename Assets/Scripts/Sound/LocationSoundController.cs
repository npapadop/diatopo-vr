using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class LocationSoundController : MonoBehaviour
{

    [Header("Listening On")]
    [SerializeField] private SceneEventChannelSO _onSceneReady = default;
    [SerializeField] private float narrationDelay = 5f;
    [SerializeField] private AudioSource narrationAudioSource;
    [SerializeField] private AudioSource sfxAudioSource;

    private void OnEnable()
    {
        _onSceneReady.OnEventRaised += PlayNarrationSound;
    }

    private void OnDisable()
    {
        _onSceneReady.OnEventRaised -= PlayNarrationSound;
    }

    private void PlayNarrationSound(GameSceneSO gameSceneSO, int extraParam)
    {
        if(extraParam == 0)
        {
            StartCoroutine(PlayNarrationSoundWithDelay());
        }
    }

    private IEnumerator PlayNarrationSoundWithDelay()
    {
        yield return new WaitForSeconds(narrationDelay);
        narrationAudioSource.Play();
    }

    public void PlaySFXClip(AudioClip sfxClip)
    {
        sfxAudioSource.PlayOneShot(sfxClip);
    }

    public void DisableAudioEffects()
    {
        sfxAudioSource.enabled = false;
    }
}
