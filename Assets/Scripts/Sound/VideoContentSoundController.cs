using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoContentSoundController : MonoBehaviour
{
    [SerializeField] private AudioSource narrativeSound;
    [SerializeField] private AudioSource sfxAudioSource;

    public void PauseNarrativeSound()
    {
        narrativeSound.Stop();
    }

    public void ResumeNarrativeSound()
    {
        narrativeSound.Play();
    }

    public void PlaySFXClip(AudioClip sfxClip)
    {
        sfxAudioSource.PlayOneShot(sfxClip);
    }

    public void LoopSFXClip()
    {
        sfxAudioSource.Play();
    }

    public void StopSFXClip()
    {
        sfxAudioSource.Stop();
    }

    public void DisableAudioEffects()
    {
        sfxAudioSource.enabled = false;
    }
}
