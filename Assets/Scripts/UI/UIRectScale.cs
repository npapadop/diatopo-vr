using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UIRectScale : MonoBehaviour
{
    private Vector3 originalScale;
    private RectTransform rectTransform;
    public float ScaleX;
    public float ScaleY;
    public float ScaleZ;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        originalScale = rectTransform.localScale;
    }

    public void ModifyRectScale()
    {
        rectTransform.localScale = new Vector3(ScaleX, ScaleY, ScaleZ);
    }

    public void OriginalRectScale()
    {
        rectTransform.localScale = originalScale;
    }

    
}
