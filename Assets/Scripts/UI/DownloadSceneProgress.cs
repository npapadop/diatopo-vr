using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class DownloadSceneProgress : MonoBehaviour
{
    public int downloadProgressInput;
    private int cachedDownloadProgressInput;
    [SerializeField] private TextMeshProUGUI progressText;

    void Start()
    {
        //progressText = GetComponent<Text>();
        cachedDownloadProgressInput = 0;
        progressText.text = "0 %";
    }

    void Update()
    {

        if (cachedDownloadProgressInput != downloadProgressInput)
        {
            cachedDownloadProgressInput = downloadProgressInput;
            progressText.text = cachedDownloadProgressInput.ToString() + " %";
        }
    }
}
