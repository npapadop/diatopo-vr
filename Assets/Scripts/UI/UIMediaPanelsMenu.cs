using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIMediaPanelsMenu : MonoBehaviour
{
    [SerializeField] private float ScaleDownPanelDuration = 0.5f;
    [SerializeField] private float ScaleUpPanelDuration = 0.8f;

    [SerializeField] GameObject _360VideosPanel;
    [SerializeField] GameObject _3DModelsPanel;
    [SerializeField] GameObject _2DVideosPanel;
    [SerializeField] Collider _360VideosCloseButton;
    [SerializeField] Collider _3DModelsCloseButton;
    [SerializeField] Collider _2DVideosCloseButton;
    [SerializeField] LocationSceneManagement _locationSceneController;
    public UnityAction HideMediaPanelAction;

    [SerializeField] LocationSoundController locationSoundController;
    [SerializeField] AudioClip _popAudioClip;

    public void Deactivate360VideosPanel()
    {
        _360VideosPanel.SetActive(false);
    }
    public void Deactivate2DVideosPanel()
    {
        _2DVideosPanel.SetActive(false);
    }
    public void Deactivate3DModelsPanel()
    {
        _3DModelsPanel.SetActive(false);
    }

    public void Enable3DModelsPanel()
    {
        _3DModelsPanel.SetActive(true);
        locationSoundController.PlaySFXClip(_popAudioClip);
        iTween.ScaleTo(_3DModelsPanel, iTween.Hash("time", ScaleUpPanelDuration, "scale", new Vector3(1f, 1f, 1f)));
    }

    public void Enable360VideosPanel()
    {
        _360VideosPanel.SetActive(true);
        locationSoundController.PlaySFXClip(_popAudioClip);
        iTween.ScaleTo(_360VideosPanel, iTween.Hash("time", ScaleUpPanelDuration, "scale", new Vector3(1f, 1f, 1f)));
    }

    public void Enable2DVideosPanel()
    {
        _2DVideosPanel.SetActive(true);
        locationSoundController.PlaySFXClip(_popAudioClip);
        iTween.ScaleTo(_2DVideosPanel, iTween.Hash("time", ScaleUpPanelDuration, "scale", new Vector3(1f, 1f, 1f)));

    }

    public void Disable3DModelsPanel()
    {
        iTween.ScaleTo(_3DModelsPanel, iTween.Hash("time", ScaleDownPanelDuration, "scale", new Vector3(0.00001f, 0.00001f, 0.00001f), "onComplete", "Hide3DModelsPanel", "onCompleteTarget", this.gameObject));
        
    }

    private void Hide3DModelsPanel()
    {
        _3DModelsPanel.SetActive(false);
        _3DModelsCloseButton.enabled = true;
        HideMediaPanelAction.Invoke();
    }

    public void Disable360VideosPanel()
    {
        iTween.ScaleTo(_360VideosPanel, iTween.Hash("time", ScaleDownPanelDuration, "scale", new Vector3(0.00001f, 0.00001f, 0.00001f), "onComplete", "Hide360VideosPanel", "onCompleteTarget", this.gameObject));
    }

    private void Hide360VideosPanel()
    {
        _360VideosPanel.SetActive(false);
        _360VideosCloseButton.enabled = true;
        HideMediaPanelAction.Invoke();
    }

    public void Disable2DVideosPanel()
    {
        iTween.ScaleTo(_2DVideosPanel, iTween.Hash("time", ScaleDownPanelDuration, "scale", new Vector3(0.00001f, 0.00001f, 0.00001f), "onComplete", "Hide2DVideosPanel", "onCompleteTarget", this.gameObject));
    }

    private void Hide2DVideosPanel()
    {
        _2DVideosPanel.SetActive(false);
        _2DVideosCloseButton.enabled = true;
        HideMediaPanelAction.Invoke();
    }

    public void NotifySceneController(object cmpPparams)
    {
        Hashtable hstbl = (Hashtable)cmpPparams;
        GameSceneSO gameSceneSO = (GameSceneSO)hstbl["gameSceneSO"];
        GameObject panelToClose = (GameObject)hstbl["panelToClose"];

        _locationSceneController.LoadContentScene(gameSceneSO);
        panelToClose.SetActive(false);


    }
}
