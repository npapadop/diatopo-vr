using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasWorldSpaceCamera : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
    }
}
