using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressLoadingController : MonoBehaviour
{
	[SerializeField] private GameObject _loadingInterface = default;

	/*[Header("Listening on")]
    [SerializeField] private BoolEventChannelSO _toggleLoadingScreen = default;
	private void OnEnable()
	{
		_toggleLoadingScreen.OnEventRaised += ToggleLoadingScreen;
	}

	private void OnDisable()
	{
		_toggleLoadingScreen.OnEventRaised -= ToggleLoadingScreen;
	}*/

	public void ToggleLoadingScreen(bool state)
	{

		_loadingInterface.SetActive(state);

	}
}
