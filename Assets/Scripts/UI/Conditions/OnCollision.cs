using System;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Calls functionality when a collision occurs
/// </summary>
public class OnCollision : MonoBehaviour
{
     public string requiredTag = string.Empty;

    [Serializable] public class CollisionEvent : UnityEvent<Collision> { }

    // When the object enters a collision
    public CollisionEvent OnEnter = new CollisionEvent();

    // When the object exits a collision
    public CollisionEvent OnExit = new CollisionEvent();

    private void OnCollisionEnter(Collision collision)
    {
        if (CanCollide(collision.gameObject))
            OnEnter.Invoke(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (CanCollide(collision.gameObject))
            OnExit.Invoke(collision);
    }

    private bool CanCollide(GameObject otherGameObject)
    {
        if(requiredTag != string.Empty)
        {
            return otherGameObject.CompareTag(requiredTag);
        }
        else
        {
            return true;
        }
    }

    private void OnValidate()
    {
        if (TryGetComponent(out Collider collider))
            collider.isTrigger = false;
    }
}
