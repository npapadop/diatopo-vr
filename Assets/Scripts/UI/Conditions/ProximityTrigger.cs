using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityTrigger : MonoBehaviour
{
    public string requiredTag = string.Empty;

    [SerializeField] private UIMediaObjectsMenuItem _3DModelsMenuItem;
    [SerializeField] private UIMediaObjectsMenuItem _360VideoMenuItem;
    [SerializeField] private UIMediaObjectsMenuItem _2DVideoMenuItem;

    private void OnTriggerEnter(Collider other)
    {
        if (!CanTrigger(other.gameObject))
            return;

        if (_3DModelsMenuItem && other.gameObject.name == _3DModelsMenuItem.GetItemObjectName())
            _3DModelsMenuItem.MenuItemEnteredTriggerArea();
        else if (_360VideoMenuItem && other.gameObject.name == _360VideoMenuItem.GetItemObjectName())
            _360VideoMenuItem.MenuItemEnteredTriggerArea();
        else
            _2DVideoMenuItem.MenuItemEnteredTriggerArea();

    }

    private bool CanTrigger(GameObject otherGameObject)
    {
        if (requiredTag != string.Empty)
        {
            return otherGameObject.CompareTag(requiredTag);
        }
        else
        {
            return true;
        }
    }
}
