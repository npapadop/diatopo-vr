using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuitCanvas : MonoBehaviour
{
    [Header("Listening to")]
    [SerializeField] private SceneEventChannelSO _onSceneReady = default;

    [Header("Broadcasting on")]
    [SerializeField] private VoidEventChannelSO _exitSceneSelcted = default;

    [Header("Offset parameters")]
    [SerializeField] private float offsetY = 0.85f;
    [SerializeField] private float offsetZ = 0.45f;
    [SerializeField] private float reverseAngle = 100f;
    [SerializeField] private GameObject exitInteractiveGO;
    [SerializeField] private GameObject exitPanelGO;
    [SerializeField] private GameObject exitText;
    private Collider _imageCollider;
    private Collider _panelCollider;
    private Image _currentPanelColor;

    [SerializeField] private AudioSource QuitAudioSource;

    private void Awake()
    {
        _imageCollider = exitInteractiveGO.GetComponent<Collider>();
        _panelCollider = exitPanelGO.GetComponent<Collider>();
        _currentPanelColor = exitPanelGO.GetComponent<Image>();
        _imageCollider.enabled = false;
        _panelCollider.enabled = false;
        exitText.SetActive(false);
        _currentPanelColor.color = new Color32(180, 180, 180, 255);
    }

    private void OnEnable()
    {
        _onSceneReady.OnEventRaised += AdjustCanvasPosition;
        _onSceneReady.OnEventRaised += AdjustButtonInteraction;
    }

    private void OnDisable()
    {
        _onSceneReady.OnEventRaised -= AdjustCanvasPosition;
        _onSceneReady.OnEventRaised -= AdjustButtonInteraction;
    }

    private void AdjustCanvasPosition(GameSceneSO gameSceneSO, int extraParam)
    {
        Vector3 cameraPosition = Camera.main.transform.localPosition;

        switch (gameSceneSO.sceneType)
        {
            case GameSceneSO.GameSceneType.Menu:
            case GameSceneSO.GameSceneType.VideoContent:
                transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y - 0.85f, cameraPosition.z + 0.45f);
                transform.localRotation = Quaternion.identity;
                break;
            case GameSceneSO.GameSceneType.Location:
                transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y - 0.78f, cameraPosition.z + 0.35f);
                transform.localRotation = Quaternion.identity;
                break;
            case GameSceneSO.GameSceneType.Video360Content:
                transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y + 0.45f, cameraPosition.z + 0.23f);
                transform.localRotation = Quaternion.Euler(reverseAngle, 0, 0);
                break;
            case GameSceneSO.GameSceneType.Model3DContent:
                transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y + 0.68f, cameraPosition.z + 0.35f);
                transform.localRotation = Quaternion.Euler(reverseAngle, 0, 0);
                break;
            default:
                transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y - offsetY, cameraPosition.z + offsetZ);
                transform.localRotation = Quaternion.identity;
                break;

        }
    }

    private void AdjustButtonInteraction(GameSceneSO gameSceneSO, int extraParam)
    {
        if (gameSceneSO.sceneType == GameSceneSO.GameSceneType.Location || gameSceneSO.sceneType == GameSceneSO.GameSceneType.Video360Content)
        {
            _imageCollider.enabled = true;
            _panelCollider.enabled = true;
        }
        else
        {
            _imageCollider.enabled = false;
            _panelCollider.enabled = false;
        }
    }

    public void OnQuitButtonEnter()
    {
        exitText.SetActive(true);
        _currentPanelColor.color = new Color32(156, 156, 156, 255);
    }

    public void OnQuitButtonExit()
    {
        exitText.SetActive(false);
        _currentPanelColor.color = new Color32(180, 180, 180, 255);
    }

    public void OnQuitButtonTriggered()
    {
        iTween.PunchPosition(exitPanelGO, iTween.Hash("time", 0.8f, "z", -0.05f, "OnComplete", "ExitScene", "OnCompleteTarget", this.gameObject));
    }

    public void ExitScene()
    {
        //Debug.Log("Raised event for exit scene");
        _exitSceneSelcted.RaiseEvent();
    }

    public void PlayQuitSFXClip(AudioClip quitSFXClip)
    {
        QuitAudioSource.PlayOneShot(quitSFXClip);
    }

}
