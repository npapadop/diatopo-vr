using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILocationMenu : MonoBehaviour
{
    [Header("Listening to")]
    [SerializeField] SceneEventChannelSO _SceneReadyChannel;

    public UIMediaObjectsMenu _MediaObjectsMenu;
    public UIMediaPanelsMenu _MediaPanelsMenu;

    void OnEnable()
    {
        _SceneReadyChannel.OnEventRaised += MediaPanelEnableOnStart; 
        _MediaObjectsMenu.Trigger3DModelsPanelAction += Show3DModelPanel;
        _MediaObjectsMenu.Trigger360VideosPanelAction += Show360VideoPanel;
        _MediaObjectsMenu.Trigger2DVideosPanelAction += Show2DVideoPanel;
        _MediaPanelsMenu.HideMediaPanelAction += HideMediaPanel;
        _MediaPanelsMenu.HideMediaPanelAction += HideMediaPanel;
        _MediaPanelsMenu.HideMediaPanelAction += HideMediaPanel;
    }

    void OnDisable()
    {
        _SceneReadyChannel.OnEventRaised -= MediaPanelEnableOnStart;
        _MediaObjectsMenu.Trigger3DModelsPanelAction -= Show3DModelPanel;
        _MediaObjectsMenu.Trigger360VideosPanelAction -= Show360VideoPanel;
        _MediaObjectsMenu.Trigger2DVideosPanelAction -= Show2DVideoPanel;
        _MediaPanelsMenu.HideMediaPanelAction -= HideMediaPanel;
        _MediaPanelsMenu.HideMediaPanelAction -= HideMediaPanel;
        _MediaPanelsMenu.HideMediaPanelAction -= HideMediaPanel;
    }

    private void HideMediaPanel()
    {
        _MediaPanelsMenu.gameObject.SetActive(false);
        _MediaObjectsMenu.gameObject.SetActive(true);
        _MediaObjectsMenu.EnableObjectsMenu();
    }


    private void Show3DModelPanel()
    {
        _MediaObjectsMenu.gameObject.SetActive(false);
        _MediaPanelsMenu.gameObject.SetActive(true);
        _MediaPanelsMenu.Enable3DModelsPanel();
    }

    private void Show360VideoPanel()
    {
        _MediaObjectsMenu.gameObject.SetActive(false);
        _MediaPanelsMenu.gameObject.SetActive(true);
        _MediaPanelsMenu.Enable360VideosPanel();
    }

    private void Show2DVideoPanel()
    {
        _MediaObjectsMenu.gameObject.SetActive(false);
        _MediaPanelsMenu.gameObject.SetActive(true);
        _MediaPanelsMenu.Enable2DVideosPanel();
    }

    public void Close3DModelPanel()
    {
        _MediaPanelsMenu.Disable3DModelsPanel(); 
    }

    public void Close360VideoPanel()
    {
        _MediaPanelsMenu.Disable360VideosPanel();
    }

    public void Close2DVideoPanel()
    {
        _MediaPanelsMenu.Disable2DVideosPanel();
    }

    private void CloseContentPanel(int[] cbParams)
    {
        int panelIndex = cbParams[0];

        if (panelIndex == 1)
            Close2DVideoPanel();
        else if (panelIndex == 2)
            Close360VideoPanel();
        else
            Close3DModelPanel();
    }

    private void MediaPanelEnableOnStart(GameSceneSO gameSceneSO, int id){
        if (id == 1)
            Show2DVideoPanel();
        else if (id == 2)
            Show360VideoPanel();
        else if (id == 3)
            Show3DModelPanel();
        else
            StartCoroutine(EnableObjectsMenuOnStart());
    }

    private IEnumerator EnableObjectsMenuOnStart()
    {
        yield return new WaitForSeconds(2);
        _MediaObjectsMenu.EnableObjectsMenu();
    }

}
