using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvasController : MonoBehaviour
{
    [Header("Listening on")]
    [SerializeField] private FloatEventChannelSO _XROriginSnapTurn_Channel;

    void OnEnable(){
        _XROriginSnapTurn_Channel.OnEventRaised += OriginSnapTurn;
    }

    void OnDisable(){
        _XROriginSnapTurn_Channel.OnEventRaised -= OriginSnapTurn;
    }

    private void OriginSnapTurn(float rotationYAngles){
        iTween.RotateTo(gameObject, iTween.Hash("y", rotationYAngles, "time", 1.5f) );
    }


}
