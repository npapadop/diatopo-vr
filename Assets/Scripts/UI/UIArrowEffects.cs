using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIArrowEffects : MonoBehaviour
{
    [SerializeField] private GameObject Arrow1;
    [SerializeField] private GameObject Arrow2;
    [SerializeField] private GameObject Arrow3;

    void Start()
    {
        Arrow1.SetActive(false);
        Arrow2.SetActive(false);
        Arrow2.SetActive(false);
    }

    public void PlayArrowsAnimation()
    {
        StartCoroutine(PlayArrowAnimation(0f,Arrow1));
        StartCoroutine(PlayArrowAnimation(1f,Arrow2));
        StartCoroutine(PlayArrowAnimation(2f,Arrow3));
    }

    public void StopArrowsAnimation()
    {
        StopAllCoroutines();
        Arrow1.SetActive(false);
        Arrow2.SetActive(false);
        Arrow3.SetActive(false);
    }

    private IEnumerator PlayArrowAnimation(float delay, GameObject arrowgo)
    {
        yield return new WaitForSeconds(delay);
        arrowgo.SetActive(true);

    }
    


}
