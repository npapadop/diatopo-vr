using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPanelOption : MonoBehaviour
{
    [SerializeField] private GameSceneSO gameSceneSO;
    [SerializeField] private UIMediaPanelsMenu _uiMediaPanelsMenu;
    [SerializeField] private GameObject contentPanel;

    public void OptionSelected()
    {
        Hashtable paramHashTable = new Hashtable();
        paramHashTable.Add("gameSceneSO", gameSceneSO);
        paramHashTable.Add("panelToClose", contentPanel);
        iTween.PunchPosition(this.gameObject, iTween.Hash("time", 1.5f, "z", 0.05f, "onComplete", "NotifySceneController", "onCompleteTarget", _uiMediaPanelsMenu.gameObject, "OnCompleteParams", paramHashTable));
    }

}
