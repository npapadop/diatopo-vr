using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeController : MonoBehaviour
{
    public Coroutine CurrentRoutine { private set; get; } = null;
    [SerializeField] private CanvasGroup _fadeCanvasGroup;
    
    [Header("Listening on")]
    [SerializeField] private FadeChannelSO _fadeChannelSO;

    private float alpha = 0.0f;

    private void OnEnable()
    {
        _fadeChannelSO.OnEventRaised += InitiateFade;
    }

    private void OnDisable()
    {
        _fadeChannelSO.OnEventRaised -= InitiateFade;
    }

    /// <summary>
	/// Controls the fade-in and fade-out.
	/// </summary>
	/// <param name="fadeIn">If false, the screen becomes black. If true, rectangle fades out and gameplay is visible.</param>
	/// <param name="duration">How long it takes to the image to fade in/out.</param>
	/// <param name="alpha">Target alpha value for canvas group to reach. Zero value when fading out.</param>
	private void InitiateFade(bool fadeIn, float duration)
	{
         StopAllCoroutines();
        if(fadeIn)
            CurrentRoutine = StartCoroutine(FadeIn(duration));
        else
            CurrentRoutine = StartCoroutine(FadeOut(duration));
	}

    private IEnumerator FadeOut(float duration)
    {
        float elapsedTime = 0.0f;

        while (alpha <= 1.0f)
        {
            SetAlpha(elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator FadeIn(float duration)
    {
        float elapsedTime = 0.0f;

        while (alpha >= 0.0f)
        {
            SetAlpha(1 - (elapsedTime / duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private void SetAlpha(float value)
    {
        alpha = value;
        _fadeCanvasGroup.alpha = alpha;
    }
}
