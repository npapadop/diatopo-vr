using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIMediaObjectsMenu : MonoBehaviour
{
    [SerializeField] private float ScaleUpMenuDuration = 1f;

    public UnityAction Trigger3DModelsPanelAction;
    public UnityAction Trigger360VideosPanelAction;
    public UnityAction Trigger2DVideosPanelAction;

    [SerializeField] UIMediaObjectsMenuItem _360VideoMenuItem;
    [SerializeField] UIMediaObjectsMenuItem _3DModelMenuItem;
    [SerializeField] UIMediaObjectsMenuItem _2DVideoMenuItem;

    void OnEnable()
    {
        if(_3DModelMenuItem)
            _3DModelMenuItem.MediaSelectedAction += Object3DModelSelected;
        if(_360VideoMenuItem)
            _360VideoMenuItem.MediaSelectedAction += Object360VideoSelected;
        if(_2DVideoMenuItem)
            _2DVideoMenuItem.MediaSelectedAction += Object2DVideoSelected;
    }

    void OnDisable()
    {
        if(_3DModelMenuItem)
            _3DModelMenuItem.MediaSelectedAction -= Object3DModelSelected;
        if(_360VideoMenuItem)
            _360VideoMenuItem.MediaSelectedAction -= Object360VideoSelected;
        if(_2DVideoMenuItem)
            _2DVideoMenuItem.MediaSelectedAction -= Object2DVideoSelected;
    }

    private void Object3DModelSelected()
    {
        Trigger3DModelsPanelAction.Invoke();

    }

    private void Object360VideoSelected()
    {
        Trigger360VideosPanelAction.Invoke();
    }

    private void Object2DVideoSelected()
    {
        Trigger2DVideosPanelAction.Invoke();
    }

    public void ResetMediaObjectsMenuItems()
    {
        if(_360VideoMenuItem)
            _360VideoMenuItem.ResetMenuItemObject();
        if(_3DModelMenuItem)
            _3DModelMenuItem.ResetMenuItemObject();
        if(_2DVideoMenuItem)
            _2DVideoMenuItem.ResetMenuItemObject();
    }

    public void EnableObjectsMenu()
    {
        ResetMediaObjectsMenuItems();
        iTween.ScaleTo(this.gameObject, iTween.Hash("time", ScaleUpMenuDuration, "scale", new Vector3(1f, 1f, 1f), "onComplete", "EnableObjectsInterructions", "onCompleteTarget", this.gameObject));
    }

    public void EnableObjectsInterructions()
    {
        if (_360VideoMenuItem)
            _360VideoMenuItem.EnableObjectInteraction();
        if (_3DModelMenuItem)
            _3DModelMenuItem.EnableObjectInteraction();
        if (_2DVideoMenuItem)
            _2DVideoMenuItem.EnableObjectInteraction();
    }

    public void DisableMediaObjectsMenuItems()
    {
        if (_360VideoMenuItem)
            _360VideoMenuItem.DisableMenuItemObject();
        if (_3DModelMenuItem)
            _3DModelMenuItem.DisableMenuItemObject();
        if (_2DVideoMenuItem)
            _2DVideoMenuItem.DisableMenuItemObject();
    }


}
