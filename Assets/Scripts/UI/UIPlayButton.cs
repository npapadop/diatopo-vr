using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayButton : MonoBehaviour
{
    [SerializeField] private GameObject ImageGO;
    private Image buttonImage;

    void Start()
    {
        buttonImage = ImageGO.GetComponent<Image>();
        buttonImage.color = new Color32(180, 180, 180, 255);
    }

    public void HoverPanelButton()
    {
        buttonImage.color = new Color32(156, 156, 156, 255);
    }

    public void ExitPanelButton()
    {
        buttonImage.color = new Color32(180, 180, 180, 255);
    }
}
