using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.UI;

public class UIMainMenuManager : MonoBehaviour
{
    [Header("Listening on")]
    [SerializeField] private float _enableInteractionDelay = 0;
    [SerializeField] private TrackedDeviceGraphicRaycaster _trackedDeviceGraphicRaycaster;


    private void Awake()
    {
        _trackedDeviceGraphicRaycaster = GetComponent<TrackedDeviceGraphicRaycaster>();
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(_enableInteractionDelay);
        _trackedDeviceGraphicRaycaster.enabled = true;
        
    }
}