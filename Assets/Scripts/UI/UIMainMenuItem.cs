using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuItem : MonoBehaviour
{
    private Vector3 originalScale;
    private Vector3 originalPosition;
    [SerializeField] private RectTransform rectTransform;
    public float AdjustScaleValue;
    public float AdjustZPosition;

    void Awake()
    {
        //rectTransform = GetComponent<RectTransform>();
        originalScale = rectTransform.localScale;
        originalPosition = rectTransform.localPosition;
    }

    public void ZoomInRectPosition(){
        rectTransform.localPosition = new Vector3(originalPosition.x, originalPosition.y, AdjustZPosition);
    }

    public void OriginalRectPosition(){
        rectTransform.localPosition = originalPosition;
    }

    public void ModifyRectScale()
    {
        rectTransform.localScale = new Vector3(AdjustScaleValue, AdjustScaleValue, originalScale.z);
    }

    public void OriginalRectScale()
    {
        rectTransform.localScale = originalScale;
    }

}
