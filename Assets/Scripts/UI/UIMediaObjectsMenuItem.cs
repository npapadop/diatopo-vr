using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class UIMediaObjectsMenuItem : MonoBehaviour
{  
    [SerializeField] private GameObject MenuItemObject;
    [SerializeField] private GameObject TextDescription;
    [SerializeField] private float ScaleDownObjectDuration = 0.8f;


    private bool _forceDropped = false;
    private Vector3 muObjectOriginalPosition;
    private Vector3 muObjectOriginalScale;
    private Quaternion muObjectOriginalRotation;
    private Collider muObjectCollider;
    private XRGrabInteractable xRGrabInteractable;
    public UnityAction MediaSelectedAction;

    protected void Awake()
    {
        xRGrabInteractable = MenuItemObject.GetComponent<XRGrabInteractable>();
        muObjectCollider = MenuItemObject.GetComponent<Collider>();
        muObjectOriginalPosition = MenuItemObject.transform.localPosition;
        muObjectOriginalRotation = MenuItemObject.transform.localRotation;
        muObjectOriginalScale = MenuItemObject.transform.localScale;
    }

    public string GetItemObjectName()
    {
        return MenuItemObject.name;
    }

    public void MenuObjectHover()
    {
        EnableTextDescription();
    }

    public void MenuObjectExit()
    {
        DisableTextDescription();
    }

    public void EnableTextDescription()
    {
        TextDescription.SetActive(true);
    }

    public void DisableTextDescription()
    {
        TextDescription.SetActive(false);
    }

    public void DisableMenuItemObject()
    {
        DisableTextDescription();
        DisableObjectInteraction();
        MenuItemObject.SetActive(false);
    }

    public void EnableMenuItemObject()
    {
        MenuItemObject.SetActive(true);
    }

    public void DisableObjectInteraction()
    {
        muObjectCollider.enabled = false;
    }

    public void EnableObjectInteraction()
    {
        muObjectCollider.enabled = true;
        xRGrabInteractable.interactionLayers = InteractionLayerMask.GetMask("Interactables");
    }

    public void MenuItemObjectGrabbed()
    {
        StopAllCoroutines();
    }

    public void ResetMenuItemObject(){
        ResetObjectTransform();
        DisableTextDescription();
        MenuItemObject.SetActive(true);
    }

    public void MenuItemObjectDropped()
    {
        if(_forceDropped)
            ResetObjectForceDropped();
        else
            StartCoroutine(ResetObjectDropped());
    }

    public void MenuItemEnteredTriggerArea()
    {
        _forceDropped = true;
        xRGrabInteractable.interactionLayers = InteractionLayerMask.GetMask("Default");
    }

    private void ResetObjectTransform()
    {
        MenuItemObject.transform.localScale = muObjectOriginalScale;
        MenuItemObject.transform.localPosition = muObjectOriginalPosition;
        MenuItemObject.transform.localRotation = muObjectOriginalRotation;
    }

    private IEnumerator ResetObjectDropped()
    {
        yield return new WaitForSeconds(0f);
        ResetObjectTransform();
    }

    private void ResetObjectForceDropped()
    {
        DisableTextDescription();
        iTween.ScaleTo(MenuItemObject, iTween.Hash("time", ScaleDownObjectDuration, "scale", new Vector3(0.00001f, 0.00001f, 0.00001f), "onComplete", "MediaPanelActivateAction", "onCompleteTarget", this.gameObject));
    }

    private void MediaPanelActivateAction(){
        _forceDropped = false;
        MenuItemObject.SetActive(false);
        ResetObjectTransform();
        MediaSelectedAction.Invoke();
    }
}
