using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICloseButton : MonoBehaviour
{
    private Image closeButtonImage;
    [SerializeField] private UILocationMenu _uiLocationMenu;
    [SerializeField] private int panelIndex;


    void Start()
    {
        closeButtonImage = GetComponent<Image>();
        closeButtonImage.color = new Color32(180, 180, 180, 255);
    }

    public void HoverClosebutton()
    {
        closeButtonImage.color = new Color32(156, 156, 156, 255);
    }

    public void ExitCloseButton()
    {
        closeButtonImage.color = new Color32(180, 180, 180, 255);
    }

    public void CloseButtonTriggered()
    {
        iTween.PunchPosition(this.gameObject, iTween.Hash("time", 0.8f, "z", 0.05f, "OnComplete", "CloseContentPanel", "OnCompleteTarget", _uiLocationMenu.gameObject, "OnCompleteParams", new int[] { panelIndex }));
    }
}
