using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class Video360PlayerController : MonoBehaviour
{
    private VideoPlayer _videoPlayer;

    [SerializeField] private GameObject _btnPlay;
    [SerializeField] private VideoContentSoundController _soundController;
    private Vector3 playOriginalScale;

    void Awake(){
        _videoPlayer = GetComponent<VideoPlayer>();
        playOriginalScale = _btnPlay.transform.localScale;
    }

    void Start()
    {
        InitializePlayer();
        _videoPlayer.loopPointReached += VideoEnded;
    }

    private void VideoEnded(VideoPlayer source)
    { 
        _btnPlay.SetActive(true);
        if(_soundController)
            _soundController.PauseNarrativeSound();
        InitializePlayer();

    }

    public void PlayPause(){
        if(_videoPlayer.isPlaying){
            _videoPlayer.Pause();
            _btnPlay.SetActive(true);
        }
        else{
            _videoPlayer.Play();
            _btnPlay.SetActive(false);
            _btnPlay.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    private void InitializePlayer()
    {
        _videoPlayer.targetTexture.Release();
        _videoPlayer.time = 0;
        _videoPlayer.Play();
        RenderTexture.active = _videoPlayer.targetTexture;
        _videoPlayer.Pause();
        RenderTexture.active = null;
    }

    public void PlayButtonClicked()
    {
        iTween.ScaleTo(_btnPlay, iTween.Hash("time", 1f, "scale", Vector3.zero, "onComplete", "PlayPause", "onCompleteTarget", this.gameObject));
    }

}
