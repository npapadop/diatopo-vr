using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;

public class VideoPlayerController : MonoBehaviour
{
    private VideoPlayer _videoPlayer;

    [SerializeField] private GameObject _playBtn;
    [SerializeField] private Slider _movieSlider;
    [SerializeField] private GameObject _textureImage;

    private RawImage rawImage;

    private bool sliderEnabled = false;
    private Vector3 playOriginalScale;


    void Awake()
    {
        _videoPlayer = GetComponent<VideoPlayer>();
        rawImage = _textureImage.GetComponent<RawImage>();
        playOriginalScale = _playBtn.transform.localScale;
    }

    void Start()
    {
        InitializePlayer();
        _videoPlayer.loopPointReached += VideoEnded;
    }

    private void VideoEnded(VideoPlayer source)
    {
        _playBtn.SetActive(true);
        InitializePlayer();
    }

    public void PlayPause()
    {
        if (_videoPlayer.isPlaying)
        {
            _videoPlayer.Pause();
            _playBtn.SetActive(true);
        }
        else
        {
            _videoPlayer.Play();
            _playBtn.SetActive(false);
            _playBtn.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    void Update()
    {
        if (_videoPlayer.isPlaying)
        {
            if (!sliderEnabled)
            {
                _movieSlider.value = (float)_videoPlayer.frame / (float)_videoPlayer.frameCount;
            }

        }
    }

    public void OnPointerDown()
    {
        sliderEnabled = true;
        _videoPlayer.Pause();
    }

    public void OnPointerUp()
    {
        float frame = (float)_movieSlider.value * (float)_videoPlayer.frameCount;
        _videoPlayer.frame = (long)frame;
        sliderEnabled = false;

        if (_playBtn.activeSelf)
            PlayButtonClicked();
        else
            _videoPlayer.Play();
    }

    public void ScreenClicked()
    {
        if (_videoPlayer.isPlaying)
        {
            PlayPause();
        }
    }

    private void InitializePlayer()
    {
        _videoPlayer.targetTexture.Release();
        _videoPlayer.time = 0;
        _videoPlayer.Play();
        rawImage.texture = _videoPlayer.targetTexture;
        _videoPlayer.Pause();
    }

    public void PlayButtonClicked()
    {
        iTween.ScaleTo(_playBtn, iTween.Hash("time", 1f, "scale", Vector3.zero, "onComplete", "PlayPause", "onCompleteTarget", this.gameObject));
    }

}
