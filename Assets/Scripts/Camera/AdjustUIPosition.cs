using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustUIPosition : MonoBehaviour
{
    private float offsetYposition;
    public GameObject[] UIElements;

    void Awake()
    {
        offsetYposition = Camera.main.transform.position.y;
        AdjustUIElementsPosition();
    }

    private void AdjustUIElementsPosition()
    {
        for(int i=0; i<UIElements.Length; i++)
        {
            if (UIElements[i]) 
                UIElements[i].transform.position = new Vector3(UIElements[i].transform.position.x, offsetYposition, UIElements[i].transform.position.z);
        }
    }
}