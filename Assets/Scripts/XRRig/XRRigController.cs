using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRRigController : MonoBehaviour
{
    [Header("Broadcasting On")]
    [SerializeField] private FloatEventChannelSO _snapTurnEventChannel;

    [SerializeField] private Transform _xrRigGameObject;
    private Vector3 currentRotation;

    void Awake()
    {
        currentRotation = _xrRigGameObject.rotation.eulerAngles;
    }
    void Update(){
        if(currentRotation != _xrRigGameObject.rotation.eulerAngles)
        {
            _snapTurnEventChannel.EventRaise(_xrRigGameObject.rotation.eulerAngles.y);
            //Debug.Log("Snap Turn Event: "+ _xrRigGameObject.rotation.eulerAngles.y);
            currentRotation = _xrRigGameObject.rotation.eulerAngles;
        }
    }



}
