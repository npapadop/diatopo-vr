using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustOriginPosition : MonoBehaviour
{

    [Header("Listening On")]
    [SerializeField] private SceneEventChannelSO _onSceneReady = default;

    [Header("Broadcasting On")]
    [SerializeField] private VectorEventChannelSO _resetOriginPositionChannel = default;

    [Header("Optional transform")]
    [SerializeField] private Transform originExtPosition = null;

    private void OnEnable()
    {
        _onSceneReady.OnEventRaised += AdjustXROriginPosition;
    }

    private void OnDisable()
    {
        _onSceneReady.OnEventRaised -= AdjustXROriginPosition;
    }

    private void AdjustXROriginPosition(GameSceneSO gameSceneSO, int extraParam)
    {
        if(originExtPosition == null){
            Vector3 cameraPosition = Camera.main.transform.localPosition;
            _resetOriginPositionChannel.OnEventRaised(new Vector3(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z));
        }
        else{
             _resetOriginPositionChannel.OnEventRaised(originExtPosition.position);
        }
    }

}
