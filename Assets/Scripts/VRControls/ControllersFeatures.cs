    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ControllersFeatures : MonoBehaviour
{
    [SerializeField] private ActionBasedControllerManager leftHandController;
    [SerializeField] private ActionBasedControllerManager rightHandController;
    private Transform leftBaseController;
    private Transform rightBaseController;
    private Transform leftDirectController;
    private Transform rightDirectController;
    private Transform leftTeleportController;
    private Transform rightTeleportController;
    [SerializeField] private GameObject XROrigin;
    private ActionBasedSnapTurnProvider _XROriginSnapTurnProvider;

    private ActionBasedControllerManager leftControllerManagerScript;
    private ActionBasedControllerManager rightControllerManagerScript;
    private XRRayInteractor leftXRInteractor;
    private XRRayInteractor rightXRInteractor;


    [Header("Listening to")]
    [SerializeField] VoidEventChannelSO _resetOriginPosition;
    [SerializeField] VectorEventChannelSO _adjustOriginPosition;
    [SerializeField] ControllersEventChannelSO _toggleControllersEventChannel;
    [SerializeField] BoolEventChannelSO _toggleSnapTurnEventChannel;
    [SerializeField] BoolEventChannelSO _toggleTurnAroundEventChannel;
    [SerializeField] ControllersEventChannelSO _toggleBaseControllersEventChannel;
    [SerializeField] ControllersEventChannelSO _toggleDirectControllersEventChannel;
    [SerializeField] ControllersEventChannelSO _toggleTeleportControllersEventChannel;
    [SerializeField] FloatEventChannelSO _rayLengthEventChannel;

    private void Awake()
    {
        leftControllerManagerScript = leftHandController.GetComponent<ActionBasedControllerManager>();
        rightControllerManagerScript = rightHandController.GetComponent<ActionBasedControllerManager>();
        leftXRInteractor = leftHandController.baseControllerGameObject.GetComponent<XRRayInteractor>();
        rightXRInteractor = rightHandController.baseControllerGameObject.GetComponent<XRRayInteractor>();
        leftBaseController = leftHandController.transform.GetChild(0);
        rightBaseController = rightHandController.transform.GetChild(0);
        leftDirectController = leftHandController.transform.GetChild(1);
        rightDirectController = rightHandController.transform.GetChild(1);
        leftTeleportController = leftHandController.transform.GetChild(2);
        rightTeleportController = rightHandController.transform.GetChild(2);
        _XROriginSnapTurnProvider = XROrigin.GetComponent<ActionBasedSnapTurnProvider>();
    }

    private void OnEnable()
    {
        _resetOriginPosition.OnEventRaised += ResetXROriginRotation;
        _adjustOriginPosition.OnEventRaised += AdjustXROriginPosition;
        _toggleControllersEventChannel.OnEventRaised += ToggleControllers;
        _toggleSnapTurnEventChannel.OnEventRaised += ToggleSnapTurnControllers;
        _toggleTurnAroundEventChannel.OnEventRaised += ToggleTurnAroundControllers;
        _toggleBaseControllersEventChannel.OnEventRaised += ToggleBaseControllers;
        _toggleDirectControllersEventChannel.OnEventRaised += ToggleDirectControllers;
        _toggleTeleportControllersEventChannel.OnEventRaised += ToggleTeleportControllers;
        _rayLengthEventChannel.OnEventRaised += ModifyRayLength;
    }

    private void OnDisable()
    {
        _resetOriginPosition.OnEventRaised -= ResetXROriginRotation;
        _adjustOriginPosition.OnEventRaised -= AdjustXROriginPosition;
        _toggleControllersEventChannel.OnEventRaised -= ToggleControllers;
        _toggleSnapTurnEventChannel.OnEventRaised -= ToggleSnapTurnControllers;
        _toggleTurnAroundEventChannel.OnEventRaised -= ToggleTurnAroundControllers;
        _toggleBaseControllersEventChannel.OnEventRaised -= ToggleBaseControllers;
        _toggleDirectControllersEventChannel.OnEventRaised -= ToggleDirectControllers;
        _toggleTeleportControllersEventChannel.OnEventRaised -= ToggleTeleportControllers;
        _rayLengthEventChannel.OnEventRaised -= ModifyRayLength;
    }

    /// <summary>
    /// Toggles the VR Base Controllers
    /// </summary>
    /// <param name="toggleLeftController">Enable or disables the left base controller.</param>
    /// <param name="toggleRightController">Enable or disables the right base controller.</param>
    private void ToggleControllers(bool toggleLeftController, bool toggleRightController)
    {
        leftHandController.gameObject.SetActive(toggleLeftController);
        rightHandController.gameObject.SetActive(toggleRightController);
    }

    /// <summary>
	/// Toggles Snap Turn of VR Controllers
	/// </summary>
	/// <param name="toggleSnapTurnControllers">Enable or disables snap turn event controller.</param>
    private void ToggleSnapTurnControllers(bool toggleSnapTurnControllers)
    {
        _XROriginSnapTurnProvider.enabled = toggleSnapTurnControllers;
    }

    /// <summary>
    /// Toggles Snap Turn of VR Controllers
    /// </summary>
    /// <param name="toggleTurnAroundControllers">Enable or disables turn around event for controllers.</param>
    private void ToggleTurnAroundControllers(bool toggleTurnAroundControllers)
    {
        _XROriginSnapTurnProvider.enableTurnAround = toggleTurnAroundControllers;
    }


    /// <summary>
    /// Toggles the VR Base Controllers
    /// </summary>
    /// <param name="toggleLeftBaseController">Enable or disables the left base controller.</param>
    /// <param name="toggleRightBaseController">Enable or disables the right base controller.</param>
    private void ToggleBaseControllers(bool toggleLeftBaseController, bool toggleRightBaseController)
    {
        leftBaseController.gameObject.SetActive(toggleLeftBaseController);
        rightBaseController.gameObject.SetActive(toggleRightBaseController);
    }

    /// <summary>
    /// Toggles the VR Direct Controllers
    /// </summary>
    /// <param name="toggleLeftDirectController">Enable or disables the left direct controller.</param>
    /// <param name="toggleRightDirectController">Enable or disables the right direct controller.</param>
    private void ToggleDirectControllers(bool toggleLeftDirectController, bool toggleRightDirectController)
    {
        leftDirectController.gameObject.SetActive(toggleLeftDirectController);
        rightDirectController.gameObject.SetActive(toggleRightDirectController);
    }

    /// <summary>
    /// Toggles the VR Teleport Controllers
    /// </summary>
    /// <param name="toggleLeftTeleportController">Enable or disables the left teleport controller.</param>
    /// <param name="toggleRightTeleportController">Enable or disables the right teleport controller.</param>
    private void ToggleTeleportControllers(bool toggleLeftTeleportController, bool toggleRightTeleportController)
    {
        leftControllerManagerScript.teleportAllowed = toggleLeftTeleportController;
        leftTeleportController.gameObject.SetActive(toggleLeftTeleportController);
        rightControllerManagerScript.teleportAllowed = toggleRightTeleportController;
        rightTeleportController.gameObject.SetActive(toggleRightTeleportController);
    }
    
    private void ModifyRayLength(float length)
    {
        leftXRInteractor.maxRaycastDistance = length;
        rightXRInteractor.maxRaycastDistance = length;
    }

    private void ResetXROriginRotation()
    { 
        XROrigin.transform.localRotation = Quaternion.identity;
        XROrigin.transform.localPosition = new Vector3(0, XROrigin.transform.localPosition.y, 0);
    }

    private void AdjustXROriginPosition(Vector3 pos)
    {
        XROrigin.transform.position = new Vector3(pos.x, pos.y, pos.z);
    }

     private void SetXROriginPosition(Vector3 pos)
    {
        XROrigin.transform.position = pos;
    }
}
