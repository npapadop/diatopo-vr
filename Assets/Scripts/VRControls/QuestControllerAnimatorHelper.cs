using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class QuestControllerAnimatorHelper : MonoBehaviour
{
    [SerializeField] InputActionReference controllerGripAction;
    [SerializeField] InputActionReference controllerTriggerAction;
    [SerializeField] InputActionReference controllerPrimaryButtonAction;
    [SerializeField] InputActionReference controllerSecondaryButtonAction;
    [SerializeField] InputActionReference controllerUIButtonAction;
    [SerializeField] InputActionReference controllerTranslateAnchorAction;
    [SerializeField] InputActionReference controllerRotateAnchorAction;

    private Animator m_animator;

    void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    private void Update()
    {
        m_animator.SetFloat("Button 1", controllerPrimaryButtonAction.action.ReadValue<float>());
        m_animator.SetFloat("Button 2", controllerSecondaryButtonAction.action.ReadValue<float>());
        m_animator.SetFloat("Button 3", controllerUIButtonAction.action.ReadValue<float>());

        m_animator.SetFloat("Joy X", controllerRotateAnchorAction.action.ReadValue<Vector2>().x);
        m_animator.SetFloat("Joy Y", controllerTranslateAnchorAction.action.ReadValue<Vector2>().y);

        m_animator.SetFloat("Grip", controllerGripAction.action.ReadValue<float>());
        m_animator.SetFloat("Trigger", controllerTriggerAction.action.ReadValue<float>());
    }

   
}