using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Vector Events Channel")]
public class VectorEventChannelSO : DescriptionBaseSO
{
    public UnityAction<Vector3> OnEventRaised;

    public void EventRaise(Vector3 value)
    {
        if (OnEventRaised != null)
            OnEventRaised.Invoke(value);
    }
}
