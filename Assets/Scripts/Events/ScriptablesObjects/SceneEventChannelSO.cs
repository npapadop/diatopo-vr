using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Events/Scene Event Channel")]
public class SceneEventChannelSO : DescriptionBaseSO
{
    public UnityAction<GameSceneSO, int> OnEventRaised;

    public void EventRaise(GameSceneSO gameSceneSO, int value)
    {
        if (OnEventRaised != null)
            OnEventRaised.Invoke(gameSceneSO,value);
    }
}
