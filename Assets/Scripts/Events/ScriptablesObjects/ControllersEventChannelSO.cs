using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for events that toggle controllers features
/// Example: Enable or disable the teleportation capability of any of the controllers
/// </summary>

[CreateAssetMenu(menuName = "Events/Toggle Controllers Feature Event Channel")]
public class ControllersEventChannelSO : DescriptionBaseSO
{
    public UnityAction<bool, bool> OnEventRaised;

    public void RaiseEvent(bool lcontrollerFeature, bool rcontrollerFeature)
    {
        if(OnEventRaised != null)
            OnEventRaised.Invoke(lcontrollerFeature,rcontrollerFeature);
    }
}
